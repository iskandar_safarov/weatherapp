//
//  OWSearch.swift
//  ows
//
//  Created by Iska on 3/4/21.
//

import Foundation

public protocol OWSearchable {
	var text: String { get }
}

public struct LookupEntry {
	public let offset: UInt32
	public let id: UInt32
}

public class OWSearch: NSObject {
	
	private class TrieNode: Equatable {

		private let key: UInt32	// Unicode scalar UTF-32
		private var index: [UInt32] = []
		private var children: [TrieNode] = []

		private var rangeStart: UInt32? { return index.first ?? children.first?.rangeStart ?? nil }
		private var rangeEnd: UInt32? { return children.last?.rangeEnd ?? index.last ?? nil }

		// x2 32-bit values: num of children and index
		private let selfSize = MemoryLayout<Character>.size + MemoryLayout<UInt32>.size * 2
		private var deepSelfSize: Int {
			return children.reduce(selfSize, { return $0 + $1.deepSelfSize })
		}
		
		private func serialise(writer: OWUtils.MemoryIO) {
			writer.write(value: key)
			writer.write(value: UInt32(children.count))
			writer.write(value: UInt32(index.count))
			index.forEach { writer.write(value: $0) }
			children.forEach { $0.serialise(writer: writer) }
		}

		private init(character: Character) { key = character.unicodeScalars.first!.value }
		
		init() { key = 0 }

		init(reader: OWUtils.MemoryIO) {
			let storedKey = reader.read(as: UInt32.self)!
			let storedChildrenCount = Int(reader.read(as: UInt32.self)!)
			let storedIndexCount = reader.read(as: UInt32.self)!
			
			for _ in 0..<storedIndexCount {
				let storedIndex = reader.read(as: UInt32.self)!
				index.append(storedIndex)
			}

			for _ in 0..<storedChildrenCount {
				children.append(TrieNode(reader: reader))
			}
			key = storedKey
		}

		func importString(iterator: inout String.Iterator, index: Int) {
			if let key = iterator.next(), let utf32 = key.unicodeScalars.first?.value {
				var node: TrieNode
				if let index = children.firstIndex(where: { return $0.key == utf32 }) {
					// Index found, add it
					node = children[index]
				} else {
					// Add child
					node = TrieNode(character: key)
					children.append(node)
				}
				node.importString(iterator: &iterator, index: index)
			} else {
				// No more chars. This leaf is the last one
				self.index.append(UInt32(index))
			}
		}

		func findRange(iterator: inout String.Iterator) -> ClosedRange<Int>? {
			if let key = iterator.next() {
				if let index = children.firstIndex(where: { return $0.key == key.unicodeScalars.first?.value }) {
					// Index found, progress
					return children[index].findRange(iterator: &iterator)
				} else {
					// Index not found
					return nil
				}
			} else {
				// No more characters
				// Flatten all children and return
				if let rangeStart = rangeStart, let rangeEnd = rangeEnd {
					return ClosedRange<Int>(uncheckedBounds: (lower: Int(rangeStart), upper: Int(rangeEnd)))
				}
				return nil
			}
		}

		func serialise() -> Data {
			let writer = OWUtils.MemoryIO()
			serialise(writer: writer)
			writer.sizeToFit()
			return writer.data
		}

		static func == (lhs: OWSearch.TrieNode, rhs: OWSearch.TrieNode) -> Bool {
			return lhs.key == rhs.key && lhs.index == rhs.index && lhs.children == rhs.children
		}
	}

	private let root: TrieNode

	static func == (lhs: OWSearch, rhs: OWSearch) -> Bool {
		return lhs.root == rhs.root
	}

	public func findRange(text: String) -> ClosedRange<Int>? {
		var it = text.lowercased().makeIterator()
		return root.findRange(iterator: &it)
	}

	func serialise() -> Data {
		return root.serialise()
	}

	public init(searchData: Data) {
		let io = OWUtils.MemoryIO(data: searchData)
		root = TrieNode(reader: io)
	}

	public init(sortedList: [OWSearchable]) {
		let root = TrieNode()
		for (index, item) in sortedList.enumerated() {
			var iterator = item.text.lowercased().makeIterator()
			root.importString(iterator: &iterator, index: index)
		}
		self.root = root
	}
	
}
