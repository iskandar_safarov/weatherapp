//
//  OWUtils.swift
//  WeatherApp
//
//  Created by Iska on 31/3/21.
//

import Foundation

public class OWUtils: NSObject {
	
	public class MemoryIO {
		public var data: Data { internalData }
		private var internalData: Data
		private var cursor: Int = 0

		public init(data: Data) {
			self.internalData = data
		}

		public init() {
			self.internalData = Data(count: 1024)
		}

		public func write<T>(value: T) {
			let valueSize = MemoryLayout.size(ofValue: value)
			let bytesLeft = internalData.count - cursor
			if bytesLeft < valueSize {
				// Grow data to 1K
				internalData.append(Data(count: 1024))
			}
			internalData.withUnsafeMutableBytes {
				if let baseAddress = $0.baseAddress {
					let pValue = baseAddress.advanced(by: cursor)
					pValue.initializeMemory(as: T.self, repeating: value, count: 1)
					cursor += valueSize
				}
			}
		}

		public func read<T>(as: T.Type) -> T? {
			let valueSize = MemoryLayout<T>.size
			let bytesLeft = internalData.count - cursor
			if bytesLeft < valueSize {
				return nil
			}
			return internalData.withUnsafeMutableBytes {
				let baseAddress = $0.baseAddress!
				let valueSize = MemoryLayout<T>.size
				let value = baseAddress.advanced(by: cursor).load(as: T.self)
				cursor += valueSize
				return value
			}
		}
		
		public func sizeToFit() {
			internalData.removeSubrange(Range<Int>(uncheckedBounds: (lower: cursor, upper: internalData.count)))
		}
	}

	public static var documentsDirectory: URL {
		get {
			return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
		}
	}

	public static var backgroundQueue: DispatchQueue {
		get {
			return DispatchQueue.global(qos: .utility)
		}
	}

}
