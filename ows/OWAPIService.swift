//
//  OWAPIService.swift
//  WeatherApp
//
//  Created by Iska on 31/3/21.
//

import CoreGraphics.CGImage

public final class OWAPIService: NSObject {

	public typealias CityDetailsCompletion = (Int, OWWeather?) -> Void
	public typealias IconCompletion = (CGImage?) -> Void

	static private let apiKey = "e390809dc8bff64fbed5ba2c42cf168a"
	static private let weatherApiUrl = "http://api.openweathermap.org/data/2.5/weather"
	static private let iconApiUrl = "http://openweathermap.org/img/wn"

	public enum Units {
		case kelvin
		case celsius
		case fahrenheit
	}
	
	private lazy var apiSession: URLSession = {
		let config = URLSessionConfiguration.default
		config.allowsCellularAccess = true
		config.allowsConstrainedNetworkAccess = true
		config.allowsExpensiveNetworkAccess = true
		return URLSession(configuration: config, delegate: nil, delegateQueue: OperationQueue.main)
	}()
	
	public static var shared: OWAPIService = {
		let service = OWAPIService()
		return service
	}()

	public func getCityDetails(city: Int, units: Units, completion: @escaping CityDetailsCompletion) {
		let temperature: String
		switch units {
		case .celsius:
			temperature = "metric"
		case .kelvin:
			temperature = "standard"
		case .fahrenheit:
			temperature = "imperial"
		}
		let requestString = String("\(OWAPIService.weatherApiUrl)?id=\(city)&appid=\(OWAPIService.apiKey)&units=\(temperature)")
		guard let requestUrl = URL(string: requestString) else {
			DispatchQueue.main.async {
				completion(city, nil)
			}
			return
		}
		let dataTask = apiSession.dataTask(with: requestUrl) { data, _, error in
			let cityObject = (data != nil) ? try? JSONDecoder().decode(OWWeather.self, from: data!) : nil
			DispatchQueue.main.async {
				completion(city, cityObject)
			}
		}
		dataTask.resume()
	}
	
	public func getWeatherIcon(icon: String, completion: @escaping IconCompletion) {
		let requestString = String("\(OWAPIService.iconApiUrl)/\(icon)@2x.png")
		guard let requestUrl = URL(string: requestString) else {
			DispatchQueue.main.async {
				completion(nil)
			}
			return
		}
		let dataTask = apiSession.dataTask(with: requestUrl) { data, _, error in
			if let data = data, let provider = CGDataProvider(data: data as CFData) {
				let image = CGImage(pngDataProviderSource: provider, decode: nil, shouldInterpolate: false, intent: .defaultIntent)
				DispatchQueue.main.async {
					completion(image)
				}
			} else {
				DispatchQueue.main.async {
					completion(nil)
				}
			}
		}
		dataTask.resume()
	}

}
