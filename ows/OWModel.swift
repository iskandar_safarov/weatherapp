//
//  OWModel.swift
//  owsTests
//
//  Created by Iska on 3/4/21.
//
import Foundation

public class OWCity: Codable, OWSearchable {
	public class Coordinates: Codable {
		public let longitude: Double
		public let latitude: Double
		private enum CodingKeys: String, CodingKey {
			case longitude = "lon"
			case latitude = "lat"
		}
	}

	public let id: Int
	public let name: String
	public let state: String?
	public let country: String
	public let coord: Coordinates
	
	public var text: String { return name }
	
}

public struct OWWeather: Codable {

	public let id: Int
	public let coord: OWCity.Coordinates

	public class Weather: Codable {
		public let id: Int
		public let main: String
		public let description: String
		public let icon: String
	}
	
	public class WeatherDetails: Codable {
		public let temperature: Double
		public let feelsLike: Double
		public let temperatureMin: Double?
		public let temperatureMax: Double?
		public let pressure: Double
		public let humidity: Double
		private enum CodingKeys: String, CodingKey {
			case temperature = "temp"
			case feelsLike = "feels_like"
			case temperatureMin = "temp_min"
			case temperatureMax = "temp_max"
			case pressure
			case humidity
		}
	}
	
	public class Wind: Codable {
		public let speed: Double?
		public let direction: Double?
		public let gust: Double?
		private enum CodingKeys: String, CodingKey {
			case speed
			case direction = "deg"
			case gust
		}
	}
	
	public class Precipitation: Codable {
		public let volume1h: Double?
		public let volume3h: Double?
		private enum CodingKeys: String, CodingKey {
			case volume1h = "1h"
			case volume3h = "3h"
		}
	}
	
	public class System: Codable {
		public let sunrise: UInt
		public let sunset: UInt
	}
	
	public let weather: [Weather]?
	public let weatherDetails: WeatherDetails
	public let visibility: Double?
	public let wind: Wind?
	public let clouds: Double?
	public let rain: Precipitation?
	public let snow: Precipitation?
	public let utcDate: Date
	public let timeZone: Int
	public let system: System

	private enum CodingKeys: String, CodingKey {
		case weather, main, visibility, wind, clouds, rain, snow, id, sys, coord
		case utcDate = "dt"
		case timeZone = "timezone"
	}

	private enum CloudsKeys: CodingKey {
		case all
	}

	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)

		id = try container.decode(Int.self, forKey: .id)
		coord = try container.decode(OWCity.Coordinates.self, forKey: .coord)
		weather = try? container.decode([Weather].self, forKey: .weather)
		weatherDetails = try container.decode(WeatherDetails.self, forKey: .main)
		visibility = try? container.decode(Double.self, forKey: .visibility)
		wind = try? container.decode(Wind.self, forKey: .wind)
		let cloudsContainer = try container.nestedContainer(keyedBy: CloudsKeys.self, forKey: .clouds)
		clouds = try? cloudsContainer.decode(Double.self, forKey: CloudsKeys.all)
		rain = try? container.decode(Precipitation.self, forKey: .rain)
		snow = try? container.decode(Precipitation.self, forKey: .snow)
		let time = try container.decode(Double.self, forKey: .utcDate)
		utcDate = Date(timeIntervalSince1970: time)
		timeZone = try container.decode(Int.self, forKey: .timeZone)
		system = try container.decode(System.self, forKey: .sys)
	}

	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(coord, forKey: .coord)
		try container.encode(weather, forKey: .weather)
		try container.encode(weatherDetails, forKey: .main)
		try container.encode(visibility, forKey: .visibility)
		try container.encode(wind, forKey: .wind)
		var cloudsContainer = container.nestedContainer(keyedBy: CloudsKeys.self, forKey: .clouds)
		try cloudsContainer.encode(clouds, forKey: .all)
		try container.encode(rain, forKey: .rain)
		try container.encode(snow, forKey: .snow)
		try container.encode(utcDate.timeIntervalSince1970, forKey: .utcDate)
		try container.encode(timeZone, forKey: .timeZone)
		try container.encode(system, forKey: .sys)
	}
}
