//
//  CityDetailsViewCell.swift
//  WeatherApp
//
//  Created by Iska on 7/4/21.
//

import UIKit
import appModel

class CityDetailsViewCell: UITableViewCell {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var valueLabel: UILabel!

}
