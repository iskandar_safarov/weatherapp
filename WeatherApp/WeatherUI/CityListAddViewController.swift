//
//  CityListAddViewController.swift
//  WeatherApp
//
//  Created by Iska on 31/3/21.
//

import UIKit
import appModel

class CityListAddViewController: UIViewController {

	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
		
		tableView.dataSource = self
		tableView.delegate = self
		searchBar.delegate = self
		tableView.register(UINib(nibName: "CellCity", bundle: nil), forCellReuseIdentifier: "cell.city")

		AMCityApp.app.searchModel.onModelUpdate = { [weak self] in
			if let strongSelf = self {
				strongSelf.tableView.backgroundView = nil
				strongSelf.tableView.reloadData()
			}
		}
		
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now().advanced(by: .milliseconds(500))) { [weak self] in
			if let strongSelf = self {
				let activity = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
				activity.startAnimating()
				activity.alpha = 0
				UIView.animate(withDuration: 1, animations: {
					activity.alpha = 1
				});
				strongSelf.tableView.backgroundView = activity
			}
		}
    }
}

extension CityListAddViewController : UISearchBarDelegate {
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		AMCityApp.app.searchModel.narrowModel(text: searchText)
	}
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		dismiss(animated: true)
	}
}

extension CityListAddViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return AMCityApp.app.searchModel.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell.city") as! CityTableViewCell
		cell.accessoryType = .none
		cell.model = AMCityApp.app.searchModel[indexPath.row]
		return cell
	}
}

extension CityListAddViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let city = AMCityApp.app.searchModel[indexPath.row] {
			AMCityApp.app.listModel.add(city: city)
		}
		dismiss(animated: true)
	}
}
