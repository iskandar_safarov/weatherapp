//
//  ViewController.swift
//  WeatherApp
//
//  Created by Iska on 31/3/21.
//

import UIKit
import appModel

class CityListTableViewController: UITableViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		self.tableView.rowHeight = 50;
		AMCityApp.app.listModel.onModelUpdate = { [weak self] in
			switch $0 {
			case .delete(let index):
				self?.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .bottom)
				break
			case .add(let index):
				self?.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
				break
			}
			self?.tableView.reloadData()
		}
		tableView.register(UINib(nibName: "CellCity", bundle: nil), forCellReuseIdentifier: "cell.city")
		self.navigationItem.rightBarButtonItem = editButtonItem
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return AMCityApp.app.listModel.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell.city") as! CityTableViewCell
		cell.model = AMCityApp.app.listModel[indexPath.row]
		return cell
	}
	
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			AMCityApp.app.listModel.delete(index: indexPath.row)
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		if let vc = storyboard.instantiateViewController(withIdentifier: "city.details") as? CityDetailsTableViewController {
			vc.model = AMCityApp.app.listModel[indexPath.row]
			navigationController?.pushViewController(vc, animated: true)
		}
	}
}
