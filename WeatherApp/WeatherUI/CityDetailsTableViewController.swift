//
//  CityDetailsTableViewController.swift
//  WeatherApp
//
//  Created by Iska on 7/4/21.
//

import UIKit
import appModel

class CityDetailsTableViewController: UITableViewController {

	var model: AMCityViewModel? {
		willSet {
			title = newValue?.name
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.allowsSelection = false
	}

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
		return model?.details.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return model?.details[section].content.count ?? 0
    }
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return model?.details[section].name
	}

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genericCell = tableView.dequeueReusableCell(withIdentifier: "details.cell", for: indexPath)
		if let cell = genericCell as? CityDetailsViewCell {
			if let content = model?.details[indexPath.section].content[indexPath.row] {
				cell.valueLabel.text = content.value
				cell.nameLabel.text = content.name
			}
		}
        return genericCell
    }
}
