//
//  CityListTableViewCell.swift
//  WeatherApp
//
//  Created by Iska on 31/3/21.
//

import UIKit
import appModel

class CityTableViewCell: UITableViewCell {
	@IBOutlet private weak var nameLabel: UILabel!
	@IBOutlet private weak var tempLabel: UILabel!
	@IBOutlet weak var countryLabel: UILabel!
	static private var temperatureFormatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = .decimal
		formatter.maximumFractionDigits = 1
		return formatter
	}()

	var model: AMCityViewModel? {
		didSet {
			oldValue?.onModelUpdate = nil
			model?.onModelUpdate = { [unowned self] in
				self.syncToModel()
			}
			self.syncToModel()
		}
	}
	
	private func countryName(fromCode: String, state: String?) -> String {
		let countryName = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: fromCode) ?? fromCode
		if let state = state, state.count > 0 {
			return countryName + " (\(state))"
		}
		return countryName
	}
	
	private func syncToModel() {
		if let model = model, let name = model.name, let country = model.country {
			nameLabel.text = name
			countryLabel.text = countryName(fromCode: country, state: model.state)
			if let temperature = model.temperature {
				tempLabel.text = CityTableViewCell.temperatureFormatter.string(from: NSNumber(value: temperature))?.appending("º")
			} else {
				tempLabel.text = nil
			}
		}
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
		editingAccessoryType = .none
    }

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
}
