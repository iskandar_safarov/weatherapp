//
//  main.swift
//  WeatherUtils
//
//  Created by Iska on 6/4/21.
//

import Foundation

class ConvertFiles {
	
	func removeDuplicates<T>(sortedArray: inout [T], compare: (_: T, _: T) -> Bool) {
		var index: Int = 0
		while index + 1 < sortedArray.count {
			for i in index+1..<sortedArray.count {
				if !compare(sortedArray[index], sortedArray[i]) {
					sortedArray.removeSubrange(Range<Int>(uncheckedBounds: (index + 1, i)))
					index = i
					break
				}
			}
		}
	}
	
	func convertJson() throws {
		
		let jsonUrl = URL(fileURLWithPath: "/Users/iska/Documents/weatherapp/WeatherUtils/city.list.min.json.gz")

		let listFileUrl = URL(fileURLWithPath: "/Users/iska/Documents/weatherapp/WeatherUtils/list.dat")
		let offsetFileUrl = URL(fileURLWithPath: "/Users/iska/Documents/weatherapp/WeatherUtils/offset.dat")
		let searchFileUrl = URL(fileURLWithPath: "/Users/iska/Documents/weatherapp/WeatherUtils/index.dat")

		if let data = try? Data(contentsOf: jsonUrl), let unpacked = try? data.gunzipped() {
			
			try? FileManager.default.removeItem(at: listFileUrl)
			FileManager.default.createFile(atPath: listFileUrl.path, contents: nil)
			
			let listFile = try FileHandle(forWritingTo: listFileUrl)

			print("Decoding JSON")
			let decoder = JSONDecoder()
			if var cities = try? decoder.decode([OWCity].self, from: unpacked) {
				print("Sorting entries by name")
				cities.sort { $0.name < $1.name }
				
				print("Indexing entries")
				let search = OWSearch(sortedList: cities)
				print("Store indices")
				try search.serialise().write(to: searchFileUrl)

				print("Re-encoding entries")
				// Now store cities in a binary form for quicker loading
				let encoder = JSONEncoder()

				let writer = OWUtils.MemoryIO()
				var fileOffset: UInt32 = 0
				cities.forEach {
					if let encoded = try? encoder.encode($0) {
						writer.write(value: LookupEntry(offset: fileOffset, id: UInt32($0.id)) )
						fileOffset = fileOffset + UInt32(encoded.count)
						listFile.write(encoded)
					}
				}

				print("Store offsets")
				try writer.data.write(to: offsetFileUrl)
				print("Done")
			}
		}
	}
	
}

let convert = ConvertFiles()
try convert.convertJson()
