//
//  OWSearchTest.swift
//  owsTests
//
//  Created by Iska on 3/4/21.
//

import XCTest
@testable import ows

class OWSearchTest: XCTestCase {
	
	struct SearchableString: OWSearchable {
		var text: String
	}

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearch() throws {
		let list = [
			SearchableString(text: "Abc"),
			SearchableString(text: "Abd"),
			SearchableString(text: "Abe"),
			SearchableString(text: "Aca"),
			SearchableString(text: "Acd"),
			SearchableString(text: "Bca"),
			SearchableString(text: "Bcat"),
			SearchableString(text: "Bcb"),
			SearchableString(text: "Ccb"),
			SearchableString(text: "Ccba"),
			SearchableString(text: "Ccbd"),
			SearchableString(text: "Ccbda"),
			SearchableString(text: "Ccd")
		]

		let search = OWSearch(sortedList: list)
		let range1 = search.findRange(text: "A")
		XCTAssertNotNil(range1)
		if let range = range1 {
			XCTAssertEqual(range.lowerBound, 0)
			XCTAssertEqual(range.upperBound, 4)
		}

		let range2 = search.findRange(text: "B")
		XCTAssertNotNil(range2)
		if let range = range2 {
			XCTAssertEqual(range.lowerBound, 5)
			XCTAssertEqual(range.upperBound, 7)
		}

		let range3 = search.findRange(text: "C")
		XCTAssertNotNil(range3)
		if let range = range3 {
			XCTAssertEqual(range.lowerBound, 8)
			XCTAssertEqual(range.upperBound, 12)
		}

		let range4 = search.findRange(text: "Ab")
		XCTAssertNotNil(range4)
		if let range = range4 {
			XCTAssertEqual(range.lowerBound, 0)
			XCTAssertEqual(range.upperBound, 2)
		}

		let range5 = search.findRange(text: "Ac")
		XCTAssertNotNil(range5)
		if let range = range5 {
			XCTAssertEqual(range.lowerBound, 3)
			XCTAssertEqual(range.upperBound, 4)
		}

		let range6 = search.findRange(text: "Aca")
		XCTAssertNotNil(range6)
		if let range = range6 {
			XCTAssertEqual(range.lowerBound, 3)
			XCTAssertEqual(range.upperBound, 3)
		}

		let range7 = search.findRange(text: "Ccbd")
		XCTAssertNotNil(range7)
		if let range = range7 {
			XCTAssertEqual(range.lowerBound, 10)
			XCTAssertEqual(range.upperBound, 11)
		}

		let range8 = search.findRange(text: "D")
		XCTAssertNil(range8)
	}
	
	func testSearchDuplicates() throws {
		let list = [
			SearchableString(text: "Abc"),
			SearchableString(text: "Abd"),
			SearchableString(text: "Abd"),
			SearchableString(text: "Ace"),
		]
		let search = OWSearch(sortedList: list)
		let range1 = search.findRange(text: "Abd")
		XCTAssertNotNil(range1)
		if let range = range1 {
			XCTAssertEqual(range.lowerBound, 1)
			XCTAssertEqual(range.upperBound, 2)
		}
	}
	
	func testSearchSerialise() throws {
		let list = [
			SearchableString(text: "Abc"),
			SearchableString(text: "Abd"),
			SearchableString(text: "Abe"),
			SearchableString(text: "Aca"),
			SearchableString(text: "Acd"),
			SearchableString(text: "Bca"),
			SearchableString(text: "Bcat"),
			SearchableString(text: "Bcb"),
			SearchableString(text: "Ccb"),
			SearchableString(text: "Ccba"),
			SearchableString(text: "Ccbd"),
			SearchableString(text: "Ccbda"),
			SearchableString(text: "Ccd")
		]

		let search = OWSearch(sortedList: list)
		let range1 = search.findRange(text: "Ccbd")
		XCTAssertNotNil(range1)
		if let range = range1 {
			XCTAssertEqual(range.lowerBound, 10)
			XCTAssertEqual(range.upperBound, 11)
		}

		let serialisedData1 = search.serialise()
		XCTAssertNotNil(serialisedData1)
		XCTAssertGreaterThan(serialisedData1.count, 0)
		
		// Try to deserialise
		let search2 = OWSearch(searchData: serialisedData1)
		XCTAssertTrue(search == search2)
		
		let range7 = search2.findRange(text: "Ccbd")
		XCTAssertNotNil(range7)
		if let range = range7 {
			XCTAssertEqual(range.lowerBound, 10)
			XCTAssertEqual(range.upperBound, 11)
		}

		let serialisedData2 = search2.serialise()
		XCTAssertNotNil(serialisedData1)
		XCTAssertEqual(serialisedData1.count, serialisedData2.count)
	}

}
