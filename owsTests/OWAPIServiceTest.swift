//
//  OpenWeatherServiceTest.swift
//  owsTests
//
//  Created by Iska on 1/4/21.
//

import XCTest
@testable import ows

class OWAPIServiceTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCityRequest() throws {
		let completionExpectation = XCTestExpectation(description: "Request completion expectation")
		let ows = OWAPIService()
		ows.getCityDetails(city: 2174003, units: .celsius) { id, city in
			XCTAssertNotNil(city)
			if let city = city {
				// Check city details
				XCTAssertEqual(city.id, 2174003)
			}
			completionExpectation.fulfill()
		}
		
		wait(for: [completionExpectation], timeout: 10)
    }
	
	func testWeatherIconRequest() throws {
		let completionExpectation = XCTestExpectation(description: "Request completion expectation")
		let ows = OWAPIService()
		ows.getWeatherIcon(icon: "10d") { image in
			XCTAssertNotNil(image)
			if let image = image {
				XCTAssertGreaterThan(image.width, 0)
				XCTAssertGreaterThan(image.height, 0)
			}
			completionExpectation.fulfill()
		}
		
		wait(for: [completionExpectation], timeout: 10)
	}

}
