//
//  OWCodableTest.swift
//  owsTests
//
//  Created by Iska on 3/4/21.
//

import XCTest
@testable import ows

class OWCodableTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
	
	func testCityEntry() throws {
		let json = """
		[
			{
				"id": 833,
				"name": "Ḩeşār-e Sefīd",
				"state": "",
				"country": "IR",
				"coord": {
					"lon": 47.159401,
					"lat": 34.330502
				}
			},
			{
				"id": 3245,
				"name": "Taglag",
				"state": "",
				"country": "IR",
				"coord": {
					"lon": 44.98333,
					"lat": 38.450001
				}
			}
		]
		"""

		let decoder = JSONDecoder()
		let data = json.data(using: .utf8)!
		let cities = try decoder.decode([OWCity].self, from: data)
		XCTAssertNotNil(cities)
		XCTAssertEqual(cities.count, 2)

		XCTAssertEqual(cities[0].id, 833)
		XCTAssertEqual(cities[0].name, "Ḩeşār-e Sefīd")
		XCTAssertNotNil(cities[0].state)
		XCTAssertEqual(cities[0].state, "")
		XCTAssertNotNil(cities[0].country)
		XCTAssertEqual(cities[0].country, "IR")
		XCTAssertNotNil(cities[0].coord)
		XCTAssertEqual(cities[0].coord.longitude, 47.159401)
		XCTAssertEqual(cities[0].coord.latitude, 34.330502)

		XCTAssertEqual(cities[1].id, 3245)
		XCTAssertEqual(cities[1].name, "Taglag")
		XCTAssertNotNil(cities[1].state)
		XCTAssertEqual(cities[1].state, "")
		XCTAssertNotNil(cities[1].country)
		XCTAssertEqual(cities[1].country, "IR")
		XCTAssertNotNil(cities[1].coord)
		XCTAssertEqual(cities[1].coord.longitude, 44.98333)
		XCTAssertEqual(cities[1].coord.latitude, 38.450001)
	}

    func testCity() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
		let json = """
		{
			"coord": {
				"lon": -122.08,
				"lat": 37.39
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"base": "stations",
			"main": {
				"temp": 282.55,
				"feels_like": 281.86,
				"temp_min": 280.37,
				"temp_max": 284.26,
				"pressure": 1023,
				"humidity": 100
			},
			"visibility": 16093,
			"wind": {
				"speed": 1.5,
				"deg": 350
			},
			"clouds": {
				"all": 1
			},
			"rain": {
				"1h": 3
			},
			"dt": 1560350645,
			"sys": {
				"type": 1,
				"id": 5122,
				"message": 0.0139,
				"country": "US",
				"sunrise": 1560343627,
				"sunset": 1560396563
			},
			"timezone": -25200,
			"id": 420006353,
			"name": "Mountain View",
			"cod": 200
		}
		"""
		let decoder = JSONDecoder()
		let data = json.data(using: .utf8)!
		let city = try decoder.decode(OWWeather.self, from: data)
		XCTAssertNotNil(city)
		XCTAssertEqual(city.id, 420006353)

		XCTAssertNotNil(city.weather)
		XCTAssertEqual(city.weather?.count, 1)
		XCTAssertEqual(city.weather?.first?.id, 800)
		XCTAssertEqual(city.weather?.first?.main, "Clear")
		XCTAssertEqual(city.weather?.first?.description, "clear sky")
		XCTAssertEqual(city.weather?.first?.icon, "01d")
		XCTAssertEqual(city.weatherDetails.temperature, 282.55)
		XCTAssertEqual(city.weatherDetails.feelsLike, 281.86)
		XCTAssertEqual(city.weatherDetails.temperatureMin, 280.37)
		XCTAssertEqual(city.weatherDetails.temperatureMax, 284.26)
		XCTAssertEqual(city.weatherDetails.pressure, 1023)
		XCTAssertEqual(city.weatherDetails.humidity, 100)
		XCTAssertEqual(city.visibility, 16093)
		XCTAssertNotNil(city.wind)
		XCTAssertEqual(city.wind?.speed, 1.5)
		XCTAssertEqual(city.wind?.direction, 350)
		XCTAssertEqual(city.wind?.gust, nil)
		XCTAssertEqual(city.clouds, 1)
		XCTAssertNotNil(city.rain)
		XCTAssertEqual(city.rain?.volume1h, 3)
		XCTAssertEqual(city.rain?.volume3h, nil)
		XCTAssertNil(city.snow)
		XCTAssertEqual(city.timeZone, -25200)
		XCTAssertEqual(city.system.sunrise, 1560343627)
		XCTAssertEqual(city.system.sunset, 1560396563)
		XCTAssertEqual(city.utcDate, Date(timeIntervalSince1970: 1560350645))
    }

}
