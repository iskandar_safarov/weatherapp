//
//  OWMemoryIOTest.swift
//  owsTests
//
//  Created by Iska on 7/4/21.
//

import XCTest
import ows

class OWMemoryIOTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMemoryIO() throws {
		struct TestStruc: Equatable {
			// Bad alignment
			let a: UInt8
			let b: UInt16
			let c: UInt32
			let d: UInt64
			static func == (lhs: Self, rhs: Self) -> Bool {
				return lhs.a == rhs.a && lhs.b == rhs.b && lhs.c == rhs.c && lhs.d == rhs.d
			}
		}
		let testStruc = TestStruc(a: 1, b: 2, c: 3, d: 4)
		let writer = OWUtils.MemoryIO()
		writer.write(value: Int(-10))
		writer.write(value: testStruc)
		writer.sizeToFit()
		XCTAssertEqual(MemoryLayout<TestStruc>.size + MemoryLayout<Int>.size, writer.data.count)
		
		let reader = OWUtils.MemoryIO(data: writer.data)
		XCTAssertEqual(reader.read(as: Int.self), -10)
		XCTAssertEqual(reader.read(as: TestStruc.self), testStruc)
    }

}
