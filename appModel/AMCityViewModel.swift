//
//  AMCityViewModel.swift
//  appModel
//
//  Created by Iska on 4/4/21.
//

import ows

public class AMCityViewModel: NSObject, Codable {
	
	public struct DataValue {
		public let name: String
		public let value: String
		init?(name: String, value: String?) {
			if let value = value {
				self.value = value
				self.name = name
				return
			}
			return nil
		}
	}
	
	public struct Section {
		public let name: String
		public let content: [DataValue]
		init(name: String, optionalContent: [DataValue?]) {
			self.name = name
			self.content = optionalContent.compactMap { $0 }
		}
	}

	public var details: [Section] = []

	public var id: Int
	public var name: String?
	public var country: String?
	public var state: String?
	public var temperature: Double?

	private enum CodingKeys: String, CodingKey {
		case id, name, country, state, temperature
	}

	public var onModelUpdate: (() -> ())?
	var onModelListUpdate: (() -> ())?

	private static var jsonDecoder: JSONDecoder = {
		return JSONDecoder()
	}()
	
	private lazy var doubleFormatter: NumberFormatter = {
		let formatter = NumberFormatter()
		formatter.numberStyle = .decimal
		formatter.maximumFractionDigits = 2
		return formatter
	}()
	
	private lazy var timeFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .none
		formatter.timeStyle = .medium
		return formatter
	}()

	private func format(value: Double?, suffix: String? = nil, digits: Int = 2) -> String? {
		doubleFormatter.maximumFractionDigits = digits
		if let value = value {
			let value = doubleFormatter.string(from: NSNumber(value: value)) ?? String(value)
			if let suffix = suffix {
				return value + " " + suffix
			}
			return value
		}
		return nil
	}
	
	private func compassDirection(value: Double?) -> String? {
		if let value = value {
			let normValue = value.truncatingRemainder(dividingBy: 360.0)
			let index = Int((normValue + 11.25) / 22.5) % 16
			let names = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
			return names[index]
		}
		return nil
	}

	func update() {
		OWAPIService.shared.getCityDetails(city: id, units: .celsius) { [weak self] _, weatherDetails in
			if let strongSelf = self {
				if let weatherDetails = weatherDetails {
					var details: [Section] = []
					details.append(Section(name: "Coordinates", optionalContent: [
						DataValue(name: "Latitude", value: strongSelf.format(value: weatherDetails.coord.latitude, digits: 3)),
						DataValue(name: "Longitude", value: strongSelf.format(value: weatherDetails.coord.longitude, digits: 3))
					]))
					
					if let weather = weatherDetails.weather?.first {
						details.append(Section(name: "Weather", optionalContent: [
							DataValue(name: "Condition", value: weather.main),
							DataValue(name: "Details", value: weather.description),
							DataValue(name: "Cloudiness", value: strongSelf.format(value: weatherDetails.clouds, suffix: "%", digits: 0)),
							DataValue(name: "Visibility", value: strongSelf.format(value: weatherDetails.visibility, suffix: "m", digits: 0))
						]))
					}

					details.append(Section(name: "Details", optionalContent: [
						DataValue(name: "Temperature", value: strongSelf.format(value: weatherDetails.weatherDetails.temperature, suffix: "º", digits: 1)),
						DataValue(name: "Feels like", value: strongSelf.format(value: weatherDetails.weatherDetails.feelsLike, suffix: "º", digits: 1)),
						DataValue(name: "Temperature, min", value: strongSelf.format(value: weatherDetails.weatherDetails.temperatureMin, suffix: "º", digits: 1)),
						DataValue(name: "Temperature, max", value: strongSelf.format(value: weatherDetails.weatherDetails.temperatureMax, suffix: "º", digits: 1)),
						DataValue(name: "Pressure", value: strongSelf.format(value: weatherDetails.weatherDetails.pressure, suffix: "hPa", digits: 1)),
						DataValue(name: "Humidity", value: strongSelf.format(value: weatherDetails.weatherDetails.humidity, suffix: "%", digits: 1))
					]))

					if let wind = weatherDetails.wind {
						details.append(Section(name: "Wind", optionalContent: [
							DataValue(name: "Speed", value: strongSelf.format(value: wind.speed, suffix: "m/s", digits: 1)),
							DataValue(name: "Direction", value: strongSelf.compassDirection(value: wind.direction)),
							DataValue(name: "Gust", value: strongSelf.format(value: wind.gust, suffix: "m/s", digits: 1)),
						]))
					}

					if let rain = weatherDetails.rain {
						details.append(Section(name: "Rain", optionalContent: [
							DataValue(name: "Volume in 1 hour", value: strongSelf.format(value: rain.volume1h, suffix: "mm", digits: 0)),
							DataValue(name: "Volume in 3 hours", value: strongSelf.format(value: rain.volume3h, suffix: "mm", digits: 0))
						]))
					}

					if let snow = weatherDetails.snow {
						details.append(Section(name: "Snow", optionalContent: [
							DataValue(name: "Volume in 1 hour", value: strongSelf.format(value: snow.volume1h, suffix: "mm", digits: 0)),
							DataValue(name: "Volume in 3 hours", value: strongSelf.format(value: snow.volume3h, suffix: "mm", digits: 0))
						]))
					}

					let timeZone = TimeZone(secondsFromGMT: weatherDetails.timeZone)
					strongSelf.timeFormatter.timeZone = timeZone

					details.append(Section(name: "Misc", optionalContent: [
						DataValue(name: "Sunrise (UTC)", value: strongSelf.timeFormatter.string(from: Date(timeIntervalSince1970: Double(weatherDetails.system.sunrise)))),
						DataValue(name: "Sunset (UTC)", value: strongSelf.timeFormatter.string(from: Date(timeIntervalSince1970: Double(weatherDetails.system.sunset)))),
						DataValue(name: "Time zone", value: timeZone?.abbreviation()),
					]))

					self?.details = details

					strongSelf.temperature = weatherDetails.weatherDetails.temperature
					strongSelf.onModelUpdate?()
					strongSelf.onModelListUpdate?()
				} else {
					// Keep updating but not quicker than in 1 second
					OWUtils.backgroundQueue.asyncAfter(deadline: DispatchTime.now().advanced(by: .seconds(1))) { [weak self] in
						self?.update()
					}
				}
			}
		}
	}
	
	public required init(from decoder: Decoder) throws {
		let container = try! decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(Int.self, forKey: .id)
		name = try? container.decodeIfPresent(String.self, forKey: .name)
		country = try? container.decodeIfPresent(String.self, forKey: .country)
		state = try? container.decodeIfPresent(String.self, forKey: .state)
		temperature = try? container.decodeIfPresent(Double.self, forKey: .temperature)
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encodeIfPresent(id, forKey: .id)
		try container.encodeIfPresent(name, forKey: .name)
		try container.encodeIfPresent(country, forKey: .country)
		try container.encodeIfPresent(state, forKey: .state)
		try container.encodeIfPresent(temperature, forKey: .temperature)
	}
	
	init(id: Int, cityData: Data) {
		self.id = id
		super.init()
		OWUtils.backgroundQueue.async {
			if let model = try? AMCityViewModel.jsonDecoder.decode(OWCity.self, from: cityData) {
				DispatchQueue.main.async { [weak self] in
					if let strongSelf = self {
						strongSelf.id = model.id
						strongSelf.name = model.name
						strongSelf.country = model.country
						strongSelf.state = model.state
						strongSelf.onModelUpdate?()
						strongSelf.onModelListUpdate?()
					}
				}
			}
		}
	}

	init(city: OWCity) {
		id = city.id
		name = city.name
		state = city.state
	}

	init(city: OWCity, weather: OWWeather) {
		id = city.id
		name = city.name
		country = city.country
		state = city.state
		temperature = weather.weatherDetails.temperature
	}
	
	init(id: Int, name: String, country: String, state: String?) {
		self.id = id
		self.name = name
		self.country = country
		self.state = state
		super.init()
		update()
	}
}
