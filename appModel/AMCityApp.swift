//
//  AMCityApp.swift
//  appModel
//
//  Created by Iska on 7/4/21.
//

import UIKit

public class AMCityApp {

	public var listModel: AMCityListViewModel { list }
	public var searchModel: AMCitySearchViewModel { search }

	private var list: AMCityListViewModel = AMCityListViewModel()
	private var search: AMCitySearchViewModel = AMCitySearchViewModel()
	
	public static var app: AMCityApp = {
		return AMCityApp()
	}()
}
