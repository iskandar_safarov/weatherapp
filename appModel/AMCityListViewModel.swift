//
//  AMCityListViewModel.swift
//  appModel
//
//  Created by Iska on 4/4/21.
//

import ows

public class AMCityListViewModel: NSObject {

	public enum UpdateAction {
		case delete(index: Int)
		case add(index: Int)
	}
	
	public var count: Int {
		list.count
	}

	public subscript(index: Int) -> AMCityViewModel? {
		return list[index]
	}
	
	public func delete(index: Int) {
		let element = list.remove(at: index)
		element.onModelListUpdate = nil
		storeList()
		onModelUpdate?(.delete(index: index))
	}

	public var onModelUpdate: ((_: UpdateAction) -> ())?

	private var list: [AMCityViewModel] = []
	private lazy var localListUrl: URL = {
		return OWUtils.documentsDirectory.appendingPathComponent("local.json")
	}()
	
	private var timer: Timer?
	
	func storeList() {
		let encoder = JSONEncoder()
		if let data = try? encoder.encode(list) {
			try? data.write(to: localListUrl)
		}
	}
	
	public func add(city: AMCityViewModel) {
		guard list.firstIndex(where: { $0.id == city.id }) == nil else { return }
		list.append(city)
		city.update()
		city.onModelListUpdate = { [weak self] in
			self?.storeList()
		}
		storeList()
		onModelUpdate?(.add(index: list.count - 1))
	}

	public override init() {
		super.init()
		if let localData = try? Data(contentsOf: localListUrl),
		   let list = try? JSONDecoder().decode([AMCityViewModel].self, from: localData)
		{
			self.list = list
			list.forEach { $0.update() }
		} else {
			// No local list is found - populate it manually
			self.list = [(2147714, "Sydney", "Australia"),
						 (4163971, "Melbourne", "Australia"),
						 (2174003, "Brisbane", "Australia")].map
				{ AMCityViewModel(id: $0.0, name: $0.1, country: $0.2, state: nil) }
		}
		self.list.forEach { $0.onModelListUpdate = { [weak self] in
			self?.storeList()
		} }

		// Make models update every 30 seconds
		self.timer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true, block: { [weak self] _ in
			self?.list.forEach { $0.update() }
			self?.storeList()
		})
	}
}

