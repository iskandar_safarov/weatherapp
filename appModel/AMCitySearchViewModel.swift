//
//  AMCitySearchViewModel.swift
//  appModel
//
//  Created by Iska on 6/4/21.
//

import ows

public class AMCitySearchViewModel: NSObject {

	class State {
		let search: OWSearch
		let lookup: [LookupEntry]
		let jsons: Data
		var range: ClosedRange<Int>?
		
		init(search: OWSearch, lookup: [LookupEntry], jsons: Data) {
			self.search = search
			self.lookup = lookup
			self.jsons = jsons
			setRange(text: nil)
		}
		func setRange(text: String?) {
			if let text = text {
				range = search.findRange(text: text)
			} else {
				range = ClosedRange<Int>(uncheckedBounds: (0, lookup.count - 1))
			}
		}
	}

	public var count: Int {
		if let state = state {
			return state.range?.count ?? 0
		}
		return 0
	}

	public func narrowModel(text: String?) {
		if let state = state {
			OWUtils.backgroundQueue.async {
				state.setRange(text: text)
				DispatchQueue.main.async { [weak self] in
					self?.onModelUpdate?()
				}
			}
		}
	}

	public subscript(index: Int) -> AMCityViewModel? {
		if let state = state {
			let indexOffset = state.range?.lowerBound ?? 0
			let absIndex = index + indexOffset
			let nextIndex = absIndex + 1
			
			let range = Range(uncheckedBounds: (Int(state.lookup[absIndex].offset),
												nextIndex == state.lookup.count ? state.jsons.count : Int(state.lookup[nextIndex].offset)))
			let data = state.jsons.subdata(in: range)
			return AMCityViewModel(id: Int(state.lookup[absIndex].id), cityData: data)
		}
		return nil
	}
	
	public var onModelUpdate: (() -> ())?
	
	private var state: State? = nil
	
	public override init() {
		super.init()
		// Load optimised indices in background as this is heavy-weight
		OWUtils.backgroundQueue.async {
			let bundle = Bundle(for: AMCityListViewModel.self)
			var lookup: [LookupEntry] = []
			let search = OWSearch(searchData: try! Data(contentsOf: URL(fileURLWithPath: bundle.path(forResource: "index", ofType: "dat")!)))
			
			print("Loading")

			if let data = try? Data(contentsOf: URL(fileURLWithPath: bundle.path(forResource: "offset", ofType: "dat")!)) {
				let length = data.count / MemoryLayout<LookupEntry>.size
				if length > 0 {
					let reader = OWUtils.MemoryIO(data: data)
					for _ in 1...length {
						lookup.append(reader.read(as: LookupEntry.self)!)
					}
				}
			}

			print("Loaded")
			let citiesData = try! Data(contentsOf: URL(fileURLWithPath: bundle.path(forResource: "list", ofType: "dat")!))

			DispatchQueue.main.async { [weak self] in
				if let strongSelf = self {
					strongSelf.state = State(search: search, lookup: lookup, jsons: citiesData)
					strongSelf.onModelUpdate?()
				}
			}
		}
	}

}
